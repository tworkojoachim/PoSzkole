package com.tomekw.poszkole.exceptions;

public class StudentLessonGroupBucketNotFoundException extends RuntimeException{

    public StudentLessonGroupBucketNotFoundException(String message) {
        super(message);
    }
}
