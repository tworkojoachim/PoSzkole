package com.tomekw.poszkole.exceptions;

public class StudentLessonBucketNotFoundException extends RuntimeException{

    public StudentLessonBucketNotFoundException(String message) {
        super(message);
    }

}
