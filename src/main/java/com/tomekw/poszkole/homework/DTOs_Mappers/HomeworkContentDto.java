package com.tomekw.poszkole.homework.DTOs_Mappers;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class HomeworkContentDto {

    private Long id;
    private String homeworkContents;
    private String comment;

}
