package com.tomekw.poszkole.lesson.studentLessonBucket;

public enum StudentPresenceStatus {

    PRESENT_NO_PAYMENT,
    PRESENT_PAYMENT,
    ABSENT_NO_PAYMENT,
    ABSENT_PAYMENT,
    UNDETERMINED
}
