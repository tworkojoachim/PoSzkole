package com.tomekw.poszkole.users;


import com.tomekw.poszkole.mailbox.Mailbox;
import com.tomekw.poszkole.users.userRole.UserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS) //dla każdej klasy dziedziczącej będzie tworzona oddzielna tabela w DB
@Table(name = "users")
@Data
@NoArgsConstructor
public abstract class User {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;


    private String name;
    private String surname;
    private String email;
    private String telephoneNumber;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;


    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "mailbox_id", unique = true)
    private Mailbox mailbox;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
    )
    private List<UserRole> roles;

    public User(String name, String surname, String email, String telephoneNumber, String username, String password, Mailbox mailbox, List<UserRole> roles) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.username = username;
        this.password = password;
        this.mailbox = mailbox;
        this.roles = roles;
    }
}
